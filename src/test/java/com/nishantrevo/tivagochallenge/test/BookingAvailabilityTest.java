package com.nishantrevo.tivagochallenge.test;

import com.github.fge.jsonschema.core.report.ListReportProvider;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.jayway.jsonpath.JsonPath;
import com.nishantrevo.commons.FileUtil;
import com.nishantrevo.commons.PropertyUtil;
import com.nishantrevo.trivagochallenge.commons.R;
import io.restassured.response.ValidatableResponse;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static com.nishantrevo.commons.CustomMatchers.*;

/**
 * Test class for making valid booking availablity request and validate success response
 */
public class BookingAvailabilityTest extends ApiTestBase{
    
    
    public String input_start_date = "2016-01-21";
    public String input_end_date = "2016-01-24";
    public int input_item_id = 1;
    
    ValidatableResponse response = null;
    
    @BeforeClass
    public void makeRequest() throws Exception{
        
        String formData =
                 FileUtil.read(R.TextFileFor.VALID_BOOKING_AVAILABILITY_REQUEST)
                 .replaceAll(R.RegexFor.START_DATE, String.format(R.KeyValueFormatFor.START_DATE, input_start_date))
                 .replaceAll(R.RegexFor.END_DATE, String.format(R.KeyValueFormatFor.END_DATE, input_end_date))
                 .replaceAll(R.RegexFor.ITEM_ID, String.format(R.KeyValueFormatFor.ITEM_ID_, input_item_id));
        
        response =
                 request.body(formData)
                 .post(PropertyUtil.get(R.KeyNameFor.ENDPOINT_BOOKING_AVAILABILITY))
                 .then();
        
        response.assertThat().statusCode(200);
        response.assertThat().body(R.GPathFor.HOTEL_ID, is(notNullValue()));
    }
    
    
    ////////////////The Response contains valid JSON
    @Test (description = "The Response contains valid JSON")
    public void response_Schema(){
        JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory
                .newBuilder()
                .setReportProvider(new ListReportProvider(LogLevel.ERROR, LogLevel.FATAL))
                .freeze();
        response.assertThat().body(matchesJsonSchemaInClasspath(R.SchemaFor.BOOKING_AVAILABILITY_RESPONSE_SUCCESS).using(jsonSchemaFactory));
    }
    
    //////////////The responded date is identical to requested date
    @Test (description = "The responded start date is identical to requested start date")
    public void start_date_in_respose_identical(){
        response.body(R.GPathFor.START_DATE, equalTo(input_start_date));
    }
    
    @Test (description = "The responded date is identical to requested date: End Date")
    public void end_date_in_respose_identical(){
        response.body(R.GPathFor.END_DATE, equalTo(input_end_date));
    }
    
    /////////////Currency is a three-letter code
    @Test(description = "Currency is a three-letter code. Matching regex ^[A-Z]{3}$")
    public void currency_is_3_letter_code(){
        String jsonStr = response.extract().body().asString();
        List<String> data = JsonPath.read(jsonStr, R.JSONPathFor.CURRENCY);
        assertThat(data.toString(), data, everyItem(isThreeLetterCode()));
    }
    
    ///////////The responded room has a name and its data format is type string
    @Test(description = "Every room has a name field.")
    public void room_has_name(){
        response.body(R.GPathFor.ROOM_TYPES_ARRAY, everyItem(hasKey("name")));
    }
    
    @Test(description = "Every room name's value type is String")
    public void room_name_is_string_type(){
        response.body(R.GPathFor.ROOM_NAME, everyItem(instanceOf(String.class)));
    }
    
    
    //////////The data type of “final_price_at_booking” is number and it is greater than 0
    @Test(description = "Data type of amount of final_price_at_booking is number")
    public void final_price_at_booking_amount_is_number_type(){
        response.body(R.GPathFor.FINAL_PRICE_AT_BOOKING_AMOUNT, everyItem(isNumeric()));
    }
    
    @Test(description = "Amount of final_price_at_booking is greater than 0")
    public void final_price_at_booking_amount_is_greater_than_zero(){
        response.body(R.GPathFor.FINAL_PRICE_AT_BOOKING_AMOUNT, everyItem(isGreaterThan(0)));
    }
    
    ///////////Bonus: Test if the “end_date” is after the “start_date”
    @Test(description = "response end_date is after start_date")
    public void end_date_is_after_start_date(){
        
        String failureMsg = String
                .format("%s [%s] should be after %s [%s]"
                , R.GPathFor.END_DATE
                , input_end_date
                , R.GPathFor.START_DATE
                , input_start_date);
        
        assertThat(failureMsg, input_end_date, isAfter(input_start_date, R.DefaultFor.DATE_FORMAT));
    
    }
    @Override
    public String toString(){
        return "[" + this.getClass().getSimpleName() + " : item_id = " + input_item_id + "]";
    }
}
