package com.nishantrevo.tivagochallenge.test;

import com.nishantrevo.commons.PropertyUtil;
import com.nishantrevo.trivagochallenge.commons.R;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.*;
import org.uncommons.reportng.HTMLReporter;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

/**
 * Base class for API tests.
 */
@Listeners({HTMLReporter.class})
public class ApiTestBase {
    
    RequestSpecification request = null;
    
    @Parameters(R.KeyNameFor.CONFIG_FILE)
    @BeforeTest
    public void initProperties(@Optional(R.DefaultFor.CONFIG_FILE) String configfile) throws IOException {
        PropertyUtil.clear();
        
        if(new File(configfile).exists())
            PropertyUtil.load(configfile);
        else
            PropertyUtil.load(ApiTestBase.class.getClassLoader().getResourceAsStream(configfile));
    }
    
    @BeforeClass
    public void initRequest(){
        request =
                given()
                        .baseUri       (PropertyUtil.get(R.KeyNameFor.TEST_URL))
                        .contentType   (ContentType.URLENC)
                        .accept        (ContentType.JSON)
                        .auth().basic  (PropertyUtil.get(R.KeyNameFor.USERNAME), PropertyUtil.get(R.KeyNameFor.PASSWORD))
                        .config        (RestAssured.config().encoderConfig(EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)));
    }

}
