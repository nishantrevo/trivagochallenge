package com.nishantrevo.tivagochallenge.test;

import com.nishantrevo.commons.PropertyUtil;
import com.nishantrevo.trivagochallenge.commons.R;
import org.testng.annotations.Factory;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.io.IOException;

/**
 * Factory class to create BookingAvailabilityTest class object
 */
public class BookingAvailabilityTestFactory {
    
    @Parameters({R.KeyNameFor.CONFIG_FILE, R.KeyNameFor.TEST_HOTEL_IDS})
    @Factory
    public Object[] createBookingAvailabilityTestInstances(@Optional(R.DefaultFor.CONFIG_FILE) String configFile, @Optional String testIds) throws IOException {
        
        
        if(testIds == null){
            //i.e. no test ids provided via <paramter> tag or as env property
            //So look config file
            PropertyUtil.load(configFile);
            testIds = PropertyUtil.get(R.KeyNameFor.TEST_HOTEL_IDS);
        }
        
        if(testIds == null){
            //i.e. test hotel ids are not config file as well
            //Get default values
            testIds = R.DefaultFor.TEST_IDS;
        }
        
        String [] id_arr = testIds.split(",");
        
        Object [] bookingAvailabilityTestClassInstances = new Object[id_arr.length];
        for(int i=0; i<id_arr.length; i++){
            BookingAvailabilityTest testClassInstance = new BookingAvailabilityTest();
            testClassInstance.input_item_id = Integer.parseInt(id_arr[i]);
            bookingAvailabilityTestClassInstances[i] = testClassInstance;
        }
        
        return bookingAvailabilityTestClassInstances;
    }
}
