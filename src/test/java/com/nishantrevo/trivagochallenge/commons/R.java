package com.nishantrevo.trivagochallenge.commons;

/**
 * String constants
 */
public class R {
    
    public class KeyNameFor{
        public static final String TEST_URL = "url.base";
        public static final String ENDPOINT_BOOKING_AVAILABILITY = "url.booking.availability";
        public static final String USERNAME = "user.name";
        public static final String PASSWORD = "user.password";
        public static final String CONFIG_FILE = "configfile";
        public static final String TEST_HOTEL_IDS = "test.hotel.ids";
    }
    
    public class RegexFor{
        public static final String START_DATE = "start_date\\s*=\\s*[\\d\\w-]*";
        public static final String END_DATE = "end_date\\s*=\\s*[\\d\\w-]*";
        public static final String ITEM_ID = "\"item_id\"\\s*:\\s*\\d*";
    }
    
    public class KeyValueFormatFor{
        public static final String START_DATE = "start_date= %s";
        public static final String END_DATE = "end_date= %s";
        public static final String ITEM_ID_ = "\"item_id\": %d";
    }
    
    public class GPathFor{
        public static final String START_DATE = "start_date";
        public static final String END_DATE = "end_date";
        public static final String ROOM_TYPES_ARRAY = "room_types_array";
        public static final String ROOM_NAME = "room_types_array.name";
        public static final String FINAL_PRICE_AT_BOOKING_AMOUNT = "room_types_array.final_price_at_booking.amount";
        public static final String ERRORS = "errors";
        public static final String HOTEL_ID = "hotel_id";
    }
    
    public class JSONPathFor{
        public static final String CURRENCY = ".currency";
    }
    
    public class SchemaFor{
        public static final String BOOKING_AVAILABILITY_RESPONSE_SUCCESS = "schemas/booking-availability-response-success.json";
    }
    
    public class TextFileFor{
        public static final String VALID_BOOKING_AVAILABILITY_REQUEST = "examples/booking-availability/request.txt";
    }
    
    public class DefaultFor{
        public static final String DATE_FORMAT = "yyyy-MM-dd";
        public static final String CONFIG_FILE = "configuration.properties";
        public static final String TEST_IDS = "1,2";
    }
    
    
}
