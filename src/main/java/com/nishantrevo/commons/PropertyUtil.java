package com.nishantrevo.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Utility class to read and get properties from Config file and run environment
 */
public class PropertyUtil {
    
    private static final Properties properties = new Properties();
    
    public static void clear(){
        properties.clear();
    }
    
    public static void load(String file) throws IOException {
        File fileObject = new File(file);
        if(!fileObject.exists()){
            String fileInClassPath = PropertyUtil.class.getClassLoader().getResource(file).getPath();
            fileObject = new File(fileInClassPath);
        }
        load(fileObject);
    }
    
    public static void load(File file) throws IOException {
        load(new FileInputStream(file));
    }
    
    public static void load(InputStream fileInputStream) throws IOException {
        properties.load(fileInputStream);
    }
    
    public static String get(String key){
        String val = properties.getProperty(key);
        if(val == null){
            val = System.getProperty(key);
        }
        return val;
    }
    
    public static void set(String key, String value){
        properties.setProperty(key, value);
    }
    
}
