package com.nishantrevo.commons;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Utility class to read contents of file to String
 */
public class FileUtil {
    
    public static String read(String file) throws IOException{
        File fileObj = new File(file);
        if(!fileObj.exists()){
            URL fileInClassPath = FileUtil.class.getClassLoader().getResource(file);
            if(fileInClassPath != null)
                fileObj = new File(fileInClassPath.getPath());
        }
        if(fileObj.exists()){
            return read(fileObj);
        }
        else
            throw new FileNotFoundException(file);
    }
    
    public static String read(URL file) throws IOException {
        return read(new File(file.getPath()));
    }
    
    public static String read(File file) throws IOException{
        String content = FileUtils.readFileToString(file, Charset.defaultCharset());
        return content;
    }
}
