package com.nishantrevo.commons;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomMatchers {
    
    /**
     * Matches String to currency regex
     * @return
     */
    public static BaseMatcher<String> isThreeLetterCode(){
        
        BaseMatcher<String> isThreeLetterCode =  new BaseMatcher<String>() {
            private static final String REGEX = "^[A-Z]{3}$";
            @Override
            public boolean matches(Object o) {
                if(o == null)
                    return false;
                String s = (String) o;
                return s.matches(REGEX);
            }
            
            @Override
            public void describeTo(Description description) {
                description.appendText("a three-letter word matching exp: "+REGEX);
            }
        };
        
        return isThreeLetterCode;
    }
    
    /**
     * Checks if given object is any of numeric java type.
     * @return
     */
    public static BaseMatcher<?> isNumeric(){
        BaseMatcher<?> isNumeric = new BaseMatcher<Object>() {
            @Override
            public boolean matches(Object o) {
                if(o == null)
                    return false;
                return o instanceof Number;
            }
            
            @Override
            public void describeTo(Description description) {
                description.appendText("a Numeric");
            }
        };
        
        return isNumeric;
    }
    
    /**
     * Checks if number is greater than given double value. Converts all numeric types to double and than compares.
     * @param num
     * @return
     */
    public static BaseMatcher<? extends Number> isGreaterThan(double num){
        BaseMatcher<? extends Number> isGreaterThan = new BaseMatcher<Number>() {
            
            @Override
            public boolean matches(Object o) {
                if(o == null)
                    return false;
                double actual = Double.parseDouble(o.toString());
                return actual > num;
            }
            
            @Override
            public void describeTo(Description description) {
                description.appendText("greater than " + num);
            }
        };
        
        return isGreaterThan;
    }
    
    /**
     * Converts date string representation using dateTimeFormat and checks if its after set Date
     * @param date
     * @param dateTimeFormat
     * @return
     */
    public static BaseMatcher<String> isAfter(String date, String dateTimeFormat){
        
        BaseMatcher<String> isAfter = new BaseMatcher<String>() {
            
            SimpleDateFormat df = new SimpleDateFormat(dateTimeFormat);
            ParseException exception = null;
            
            @Override
            public boolean matches(Object o) {
                if(o == null || !(o instanceof String))
                    return false;
                try {
                    String afteDateStr = (String) o;
                    Date after = df.parse(afteDateStr);
                    Date before = df.parse(date);
                    return after.after(before);
                } catch (ParseException e) {
                    exception = e;
                    return false;
                }
            }
    
            @Override
            public void describeTo(Description description) {
                if(exception == null){
                    description.appendText("after " + date);
                }
                else {
                    description.appendText("failed with exception: " + exception.getMessage());
                }
            }
        };
        
        return isAfter;
    }
    
}
