# Trivago Test Assignment

# Table of Contents
- Test Plan
- Test Project structure
- Assumptions / Test Execution pre-requisites
- Running Tests
- Reporting
- Alter Test Suite

## Test Plan
| Specification | TestCase | Description/Comment | 
| -------- | ----------- |------|
| 1. The Response contains valid JSON | 1.1. The Response contains valid JSON  | Validate schema of success response| 
| 2. The responded date is identical to requested date | 2.1. The responded start date is identical to requested start date |
|  | 2.2 The responded end date is identical to requested end date |
| 3. Currency is a three-letter code| 3.1. Currency is a three-letter code. Matching regex \^[A-Z]{3}$ | Currency should be of 3 letters in upper case A-Z
| 4. The responded room has a name and its data format is type string | 4.1. Every room has a name field.|
||4.2. Every room name's value type is String|
|5. The data type of “final_price_at_booking” is number and it is greater than 0| 5.1. Data type of amount of final_price_at_booking is number| Since, "final_price_at_booking" is a Object type which contains field "amount" of number type. This check is put on "amount" field.
|| 5.2. Amount of final_price_at_booking is greater than 0| Same as 5.1
|6. Bonus: Test if the “end_date” is after the “start_date”| 6.1. end_date in request is after start_date | Since tests 2.1 and 2.2 check request dates are same as response, checking end_date is after start_date in request would validate the same for response. |

## Test Project structure
Test project is maven project with following major libraries:
- **TestNG** : For structuring tests
- **Rest-Assured** : For making rest calls and validations.
- **ReportNG** : For generating test reports.

| Directory : Package | File | Description |
|---|---|---|
|./|pom.xml| maven project definition|
|./|testng.xml|test suite xml|
|./src/main/java : com.nishantrevo.commons|CustomMatchers.java| definitions of custom hamcrest matchers |
||FileUtil.java| static utility to read contents of file and return as String|
||PropertyUtil.java| static utility to read and get properties from config/properties file|
|./src/test/java : com.nishantrevo.trivagochallenge.commons|R.java| Contains String constants|
|./src/test/java : com.nishantrevo.tivagochallenge.test|ApiTestBase.java|Base class for api tests. Sets up PropertyUtil and inits request object|
|./src/test/java : com.nishantrevo.tivagochallenge.test|BookingAvailabilityTest.java| Test class to make valid booking availability request and verify success response|
|./src/test/java : com.nishantrevo.tivagochallenge.test|BookingAvailabilityTestFactory.java|Factory class to create dynamic instances of BookingAvailabilityTest class|
|./src/test/resouces/|configuration.properties|properties file |
|./src/test/resouces/schemas| -- | schema definition files for booking availability response and nested objects|
|./src/test/resources/examples/booking-availability/|request.txt|valid form data for booking availability request|

## Assumptions / Test Execution pre-requisites
- maven, java-8+ is installed
- ./src/test/resources/schemas directory contains the valid and updated schemas definitions files for booking-availablity and other referenced schemas.

## Running Test
##### Via Commandline
- Open terminal and go to project root folder.
- run **mvn clean test**
- all tests in testng.xml will be executed

##### Via TestNG plugin in Eclipse/IntelliJ
- Make sure TestNG plugin is installed
- Direclty run BookingAvailabilityTest.java or BookingAvailabilityTestFactory.java in run or debug mode by right clicking on class/method name and choose run
- Similarly, testng.xml file can also be executed.

## Reporting
For every test suite execution, ReportNG report will be generated:
- If execution by **mvn clean test**: ./target/surefire-reports/html/index.html
- If execution by TestNG plugin of **Eclipse/IntelliJ**: ./test-output/html/index.html

## Alter Test Suite
- testng.xml defines parameter tag **test.hotel.ids** = "1,2,3,4". So Factory would use these ids to create 4 instances of BookingAvailablityTest class. 1 for each id. Change these values to run for different set of ids.
- If **test.hotel.ids** is not defined in testng.xml, factory would look for its value in configuration.properties. If not found there as well, default ids defined in R.java -> DefaultFor -> TEST_IDS will be picked up.


